/*
* sercom0usart.c
*
* For use on a SAM D20 Xplained Pro
*
* Created: 3/17/2018 01:13:11
* Author : Larry larryvc based on code by Uwe Uwe60
*/

#include "sam.h"
#include "stdbool.h"

// BAUD calculation
#define CLOCKRATE 47972352
#define BAUDRATE 921600
//#define BAUD 65536UL - ((uint64_t)65536 * 16 * BAUDRATE) / CLOCKRATE
//#define BAUD (65536 - ((65536 * 16.0f * BAUDRATE) / CLOCKRATE))

#define REC_ERR 2

extern uint8_t sercom0_errors, sercom0_char_received, sercom0_data;

// Using interrupts for receive and transmit
void SERCOM0_Handler()
{
	if(SERCOM0->USART.INTFLAG.bit.RXC)	//character received?
	{
		sercom0_char_received = true;
		sercom0_data = SERCOM0->USART.DATA.reg;
		

		if(SERCOM0->USART.STATUS.bit.FERR | SERCOM0->USART.STATUS.bit.BUFOVF)	//receive errors happened?
		{
			SERCOM0->USART.STATUS.reg = SERCOM_USART_STATUS_FERR | SERCOM_USART_STATUS_BUFOVF;	//reset errors (framing and buffer overflow error)
			sercom0_errors |= REC_ERR;	//set global rec error flag in my global error variable not to use the char
		}

	}

	if (SERCOM0->USART.INTFLAG.bit.TXC)
	{
		while ((SERCOM0->USART.INTFLAG.bit.DRE) == 0);
		SERCOM0->USART.INTFLAG.reg |= SERCOM_USART_INTFLAG_TXC;
	}
}

char Sercom0_putchar(char ch)
{
	while ((SERCOM0->USART.INTFLAG.bit.DRE) == 0);
	SERCOM0->USART.DATA.reg = ch;
	return ch;
}

char Sercom0_getchar(char ch)
{
	while ((SERCOM0->USART.INTFLAG.bit.RXC) == 0);
	return sercom0_data = SERCOM0->USART.DATA.reg;
}


void Sercom0_Init(void)
{
	
	
	
	
	// Clock Setup
	SERCOM0->USART.CTRLA.bit.SWRST = 1;
	while ( SERCOM0->USART.CTRLA.bit.SWRST || SERCOM0->USART.SYNCBUSY.bit.SWRST );
	
	// Power Management

	// Enable peripheral clock for SERCOM0
	// APBC Mask SERCOM0 SERCOM0 APB Clock Enable is enabled
	// #define REG_PM_APBCMASK            (*(RwReg  *)0x40000420UL) /**< \brief (PM) APBC Mask */
	//REG_PM_APBCMASK |= PM_APBCMASK_SERCOM0;
	PM->APBCMASK.reg |= PM_APBCMASK_SERCOM0;
	//PM->APBCMASK.bit.SERCOM0_ = 1;
	
	
	// Set up GCLK - Generic Clock Controller
	// GCLK_CLKCTRL - GEN Generic Clock Generator is GCLKGEN0
	// GCLK_CLKCTRL - ID Generic Clock Selection ID is GCLK_SERCOM0_CORE
	// GCLK_CLKCTRL - CLKEN Clock Enable is enabled
	// #define REG_GCLK_CLKCTRL           (*(RwReg16*)0x40000C02UL) /**< \brief (GCLK) Generic Clock Control */
	//GCLK->CLKCTRL.reg = GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID_SERCOM0_CORE | GCLK_CLKCTRL_CLKEN;
	GCLK->CLKCTRL.reg = 
			GCLK_CLKCTRL_ID(SERCOM0_GCLK_ID_CORE) | GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0;
			
	while ( GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY ) ; // Wait for synchronization

	
	
	//PORT->Group[0].DIRCLR.reg |= PORT_PA11;	// RX as input
	//PORT->Group[0].PINCFG[PORT_PA11].reg |= PORT_PINCFG_PMUXEN;
	//PORT->Group[0].PMUX[PORT_PA11].bit.PMUXO = PORT_PMUX_PMUXO_C_Val;
	//
	//PORT->Group[0].DIRSET.reg |= PORT_PA10;	// TX as output
	//PORT->Group[0].OUTSET.reg |= PORT_PA10;	// TX idle state is high
	//PORT->Group[0].PINCFG[PORT_PA10].reg |= PORT_PINCFG_PMUXEN;
	//PORT->Group[0].PMUX[PORT_PA10].bit.PMUXE = PORT_PMUX_PMUXE_C_Val;

	// set port multiplexer for peripheral TX
	// =======================================
	//uint32_t temp = (PORT->Group[0].PMUX[10>>1].reg) & PORT_PMUX_PMUXO_C_Val;
	//PORT->Group[0].PMUX[10>>1].reg = temp | PORT_PMUX_PMUXE_C_Val;
	
	PORT->Group[0].DIRSET.bit.DIRSET = 1 << 10;	// TX as output
	PORT->Group[0].OUTSET.bit.OUTSET = 1 << 10;	// TX idle state is high
	//PORT->Group[0].PINCFG[10].reg = PORT_PINCFG_PMUXEN ; // Enable port mux
	PORT->Group[0].PINCFG[10].bit.INEN = 1;
	PORT->Group[0].PINCFG[10].bit.PMUXEN = 1;
	PORT->Group[0].PMUX[10/2].bit.PMUXE = PORT_PMUX_PMUXE(2);
	
	
	//PORT->Group[0].PINCFG[11].reg = PORT_PINCFG_PMUXEN ; // Enable port mux
	
	//uint32_t temp = (PORT->Group[0].PMUX[11>>1].reg) & PORT_PMUX_PMUXO_C_Val;
	//PORT->Group[0].PMUX[11>>1].reg = temp | PORT_PMUX_PMUXE_C_Val;
	
	
	
	PORT->Group[0].DIRCLR.bit.DIRCLR = 1 << 11;
	PORT->Group[0].PINCFG[11].bit.INEN = 1;
	PORT->Group[0].PINCFG[11].bit.PMUXEN = 1;
	//PORT->Group[0].PINCFG[11].reg = PORT_PINCFG_PMUXEN | PORT_PINCFG_INEN ; // Enable port mux
	PORT->Group[0].PMUX[11/2].bit.PMUXO = 0x02;
	
	
	
//
	//
	//// PORT Setup
//
	//// SERCOM0 PAD[2] PORT_PA10 is TXD, SERCOM0 PAD[3] PIN_PA11 is RXD
	//// Set PAD[2] PORT_PA10 TXD to output
	////REG_PORT_DIRSET0 = PORT_PA10;
	//PORT->Group[0].DIRSET.reg |= 1 << 10; //output 
	////PORT
	//
//
	//// Set PMUXEN bit for both PINCFG24 PIN_PA24 TXD and PINCFG25 PIN_PA25 RXD
	//// 24 8 bit registers offset to PINCFG24, pointer is 16 bit to handle int value, (32 - 24 * 2) = 12 16 bit pointer offset
	//// Uses 4 instructions, 7 clock cycles, 12 bytes including pointer address
	//// #define REG_PORT_PINCFG0           (*(RwReg  *)0x41004440UL) /**< \brief (PORT) Pin Configuration 0 */
	//// Faster and shorter than
	//PORT->Group[0].PINCFG[10].bit.INEN = 1; //input enabled for tx on usart
	//PORT->Group[0].PINCFG[10].bit.PMUXEN  = 1; //enable pmux
	//PORT->Group[0].PMUX[10 >> 1].bit.PMUXE = PORT_PMUX_PMUXE_C_Val;
	//PORT_PINCFG_PMUXEN 
	//PORT->Group[0].DIRCLR.reg |= (1 << 11);       // Set RX Pin direction to input
	//PORT->Group[0].PINCFG[11].reg |= PORT_PINCFG_INEN;    // Set RX Pin config for input enable
	//PORT->Group[0].PINCFG[11].reg &= ~PORT_PINCFG_PULLEN;   // enable pullup/down resistor
	//PORT->Group[0].PINCFG[11].reg |= PORT_PINCFG_PMUXEN;   // enable PMUX
	//PORT->Group[0].PMUX[11>>1].bit.PMUXO = PORT_PMUX_PMUXO_C_Val; // Set the PMUX bit (if pin is even, PMUXE, if odd, PMUXO)
	
	//PORT->Group[0].PINCFG[11].bit.PMUXEN  = 1;
	//PORT->Group[0].PINCFG[10].reg |= PORT_PINCFG_PMUXEN;
	//PORT->Group[0].PINCFG[11].reg |= PORT_PINCFG_PMUXEN;
	//*((RwReg16 *)&REG_PORT_PINCFG0 + 12) = (PORT_PINCFG_PMUXEN) | (PORT_PINCFG_PMUXEN << 8);

	// Set PMUX12 to peripheral function C for PIN_PA24 TXD and PIN_PA25 RXD
	// 12 8 (4+4) bit registers offset to PMUX12
	// Uses 3 instructions, 5 clock cycles, 10 bytes including pointer address
	// #define REG_PORT_PMUX0             (*(RwReg  *)0x41004430UL) /**< \brief (PORT) Peripheral Multiplexing 0 */
	// Same result as PORT->Group[0].PMUX[12].reg = PORT_PMUX_PMUXO_C | PORT_PMUX_PMUXE_C;
	//PORT->Group[0].PMUX[10].reg = PORT_PMUX_PMUXO_C | PORT_PMUX_PMUXE_C;
	//*((RwReg8 *)&REG_PORT_PMUX0 + 12) = PORT_PMUX_PMUXO_C | PORT_PMUX_PMUXE_C;



	// SERCOM0 Setup

	// SERCOM0 USART CTRLA register setup
	// Disables SERCOM0 registers Enable-Protection so we can write to all the SERCOM0 registersr
	// SERCOM0_USART_CTRLA - Mode = USART with internal clock
	// SERCOM0_USART_CTRLA - RUNSTDBY = Generic clock is enabled in all sleep modes. Any interrupt can wake up the device.
	// SERCOM0_USART_CTRLA - TXPO assigned to PAD2
	// SERCOM0_USART_CTRLA - RXPO assigned to PAD3
	// SERCOM0_USART_CTRLA - DORD = LSB is transmitted first
	// #define REG_SERCOM0_USART_CTRLA    (*(RwReg  *)0x42001400UL) /**< \brief (SERCOM0) USART Control A */
	//SERCOM0->USART.CTRLA.reg =SERCOM_USART_CTRLA_MODE_USART_INT_CLK | SERCOM_USART_CTRLA_RUNSTDBY | SERCOM_USART_CTRLA_TXPO(2)
	//| SERCOM_USART_CTRLA_RXPO(3) | SERCOM_USART_CTRLA_DORD;
	SERCOM0->USART.CTRLA.reg = 
			//SERCOM_USART_CTRLA_CPOL |
			SERCOM_USART_CTRLA_DORD | 
			SERCOM_USART_CTRLA_MODE_USART_INT_CLK | 
			SERCOM_USART_CTRLA_RUNSTDBY |
			SERCOM_USART_CTRLA_RXPO(3) | 
			SERCOM_USART_CTRLA_TXPO(1); //look at the txpo table in the datasheet, it doesn't match the pad number
	
	
	//REG_SERCOM0_USART_CTRLA = SERCOM_USART_CTRLA_MODE_USART_INT_CLK | SERCOM_USART_CTRLA_RUNSTDBY | SERCOM_USART_CTRLA_TXPO(2)
								//| SERCOM_USART_CTRLA_RXPO(3) | SERCOM_USART_CTRLA_DORD;
	while(SERCOM0->USART.SYNCBUSY.bit.ENABLE);



	// SERCOM0 USART BAUD register setup
	// #define REG_SERCOM0_USART_BAUD     (*(RwReg16*)0x4200140AUL) /**< \brief (SERCOM0) USART Baud */
	SERCOM0->USART.BAUD.reg = 65535.0f * ( 1.0f - (16.0 * (float)(BAUDRATE)) / (float)(CLOCKRATE));

	// SERCOM0 USART CTRLB register setup
	// TXEN is enabled
	// RXEN is enabled
	// DORD = LSB is transmitted first
	// #define REG_SERCOM0_USART_CTRLB    (*(RwReg  *)0x42001404UL) /**< \brief (SERCOM0) USART Control B */
	//SERCOM0->USART.CTRLB.reg = 
			//SERCOM_USART_CTRLB_CHSIZE(0) |	// 8 bit character size
			//SERCOM_USART_CTRLB_TXEN | 
			//SERCOM_USART_CTRLB_RXEN;
	SERCOM0->USART.CTRLB.bit.TXEN = 1;
	SERCOM0->USART.CTRLB.bit.RXEN = 1;
	SERCOM0->USART.CTRLB.bit.CHSIZE = SERCOM_USART_CTRLB_CHSIZE(0);
	
	while (SERCOM0->USART.SYNCBUSY.bit.CTRLB) ;

	// Enable SERCOM0 interrupts
	// RXC Receive Complete Interrupt Enabled
	// TXC Transmit Complete Interrupt Enabled
	// #define REG_SERCOM0_USART_INTENSET (*(RwReg8 *)0x4200140DUL) /**< \brief (SERCOM0) USART Interrupt Enable Set */
	//SERCOM0->USART.INTENSET.reg = SERCOM_USART_INTENSET_RXC | SERCOM_USART_INTENSET_TXC;
	SERCOM0->USART.INTENSET.bit.TXC = 1;
	SERCOM0->USART.INTENSET.bit.RXC = 1;
	
	

	// Enable SERCOM0 NVIC Interrupt
	NVIC_EnableIRQ(SERCOM0_IRQn);

	// Enable SERCOM0
	// #define REG_SERCOM0_USART_CTRLA    (*(RwReg  *)0x42001400UL) /**< \brief (SERCOM0) USART Control A */
	//SERCOM0->USART.CTRLA.reg |= SERCOM_USART_CTRLA_ENABLE;
	SERCOM0->USART.CTRLA.bit.ENABLE = 1;
	// Wait for sercom to enable
	while(SERCOM0->USART.SYNCBUSY.bit.ENABLE) ;
	
	
}
