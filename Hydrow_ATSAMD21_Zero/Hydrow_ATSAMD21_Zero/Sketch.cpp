﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

/*
 Name:    Hydrow_zero.ino
 Created: 9/1/2021 5:22:51 PM
 Author:  jasonkelly
*/
#include "INA237.h"
//Beginning of Auto generated function prototypes by Atmel Studio

//End of Auto generated function prototypes by Atmel Studio

void Sercom0_Init(void);
char Sercom0_putchar(char ch);
char Sercom0_getchar();


#define DEBUG_READABLE
//#define DEBUG_TO_HYDROW_COM
//#define DEBUG_DAC
//#define INA237_CONNECTED
//#define DEBUG_TRACE

#define timeoutMax 200

//#define DAC_OUTPUT_PIN 3
#define DAC_OUTPUT_PIN A0
#define dacRes 10
#define hydroMaxNm 30.0


// CURRENT_LSB = Imax / 2^15
// Imax ~ 10.75 A
#define CURRENT_LSB 0.000328044 
// SHUNT_CAL = 819.2*10^6 x CURRENT_LSB x Rshunt
// Rshunt = 0.015 OHM
// SHUNT_CAL = 4031 = 0x0FBF
#define INA237_SHUNT_CAL 0x0FBF 

//values to scale readings by to get them into the dame scale as the 10 bit DAC
#define INA237_VS_DAC_SCALE_CONV 32
#define HYDROW_DAC_SCALE_CONV 34.1    //calculation is [ (2^10 - 1) / hydrowMax ]. Cant do mathmatically due to floating point round-off error

#define INA237_CS_RESISTOR 0.015

//faster
#define PORT PORT_IOBUS



#ifdef DEBUG_READABLE
#define intervalDEBUG 500 //human readable
#else
#define intervalDEBUG 500 //print to graph
#endif // DEBUG_READABLE

//we're going straight unity here because we want to access it as fast as possible. 
//we area only hitting speeds around 1.4~1.8 Mhz, so our access cycle is a little over 50 us wide.
//And the chip has a sample rate of 50 us, so when we're ready, it's ready
#define intervalINA237 2 

volatile uint16_t timerCounterDEBUG = 0;
volatile uint8_t timerCounterINA237 = 0;
volatile uint32_t timerCounterDELAY = 0;


#ifdef DEBUG_TRACE
	#define IS_MTB_ENABLED \
	REG_MTB_MASTER & MTB_MASTER_EN
	#define DISABLE_MTB \
	REG_MTB_MASTER = REG_MTB_MASTER & ~MTB_MASTER_EN
	#define ENABLE_MTB \
	REG_MTB_MASTER = REG_MTB_MASTER | MTB_MASTER_EN

	__attribute__((aligned(1024)))
	volatile char __tracebuffer__[1024];
	volatile int __tracebuffersize__ = sizeof(__tracebuffer__);
	void InitTraceBuffer()
	{
		int index = 0;
		uint32_t mtbEnabled = IS_MTB_ENABLED;
		DISABLE_MTB;
		for(index =0; index<1024; index++)
		{
			__tracebuffer__[index];
			__tracebuffersize__;
		}
		if(mtbEnabled)
		ENABLE_MTB;
	}
#endif



void TC3_Handler()
{
    timerCounterDEBUG++;
    timerCounterINA237++;
    timerCounterDELAY++;

    // Acknowledge the interrupt (clear MC0 interrupt flag to re-arm)
    TC3->COUNT16.INTFLAG.reg |= 0b00010000;
}


void TC3_Delay_ms(uint32_t ms)
{
    timerCounterDELAY = 0;
    while (timerCounterDELAY < (ms * 50)) asm("nop");
}


char* _dtostrf(double val, signed char width, unsigned char prec, char* sout) {
	char fmt[20];
	sprintf(fmt, "%%%d.%df", width, prec);
	sprintf(sout, fmt, val);
	return sout;
}

void setup()
{
}



void loop()
{
#ifdef DEBUG_TRACE

	InitTraceBuffer();
#endif


    void __disable_irq(void);
    // Configure asynchronous clock source
    GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID_TCC2_TC3_Val;    // select TC3 peripheral channel
    GCLK->CLKCTRL.reg |= GCLK_CLKCTRL_GEN_GCLK0;        // select source GCLK_GEN[0]
    GCLK->CLKCTRL.bit.CLKEN = 1;            // enable TC3 generic clock

    // Configure synchronous bus clock
    PM->APBCSEL.bit.APBCDIV = 0;            // no prescaler
    PM->APBCMASK.bit.TC3_ = 1;                // enable TC3 interface

    // Configure Count Mode (16-bit)
    TC3->COUNT16.CTRLA.bit.MODE = 0x0;

    // Configure Prescaler for divide by 16
    TC3->COUNT16.CTRLA.bit.PRESCALER = TC_CTRLA_PRESCALER_DIV16_Val;

    // Configure TC3 Compare Mode for compare channel 0
    TC3->COUNT16.CTRLA.bit.WAVEGEN = 0x1;            // "Match Frequency" operation

    // Initialize compare value for 20uS
    TC3->COUNT16.CC[0].reg = 15;

    // Enable TC3 compare mode interrupt generation
    TC3->COUNT16.INTENSET.bit.MC0 = 0x1;    // Enable match interrupts on compare channel 0 

    // Enable TC3
    TC3->COUNT16.CTRLA.bit.ENABLE = 1;

    // Wait until TC3 is enabled
    while (TC3->COUNT16.STATUS.bit.SYNCBUSY == 1);

    /* Set TC3 Interrupt Priority to Level 3 */
    NVIC_SetPriority(TC3_IRQn, 2);

    /* Enable TC3 NVIC Interrupt Line */
    NVIC_EnableIRQ(TC3_IRQn);

    //NVMCTRL->CTRLB.bit.RWS = NVMCTRL_CTRLB_RWS_DUAL_Val;
    void __enable_irq(void);

    //set up a debug timing port to show clock speed
    //set port to continuous sampling
    PORT_IOBUS->Group[0].DIRSET.reg = PORT_PA14;


    ina237_t INA237;
    uint16_t configValue;
    char debugStringBuf[100];
    char dataPrintBuf[40];
    char readChar;
    char hydrowCharArray[20];
    uint8_t hydrowCharArrayIndex = 0;
    String hydrowString;
    String debugString;
    char formattedPrint0[10], formattedPrint1[10];
    bool debugInputComplete = false;
    uint8_t timeoutCounter;
    uint16_t loopCount = 0;
    bool DEBUG_DAC = true;
	uint8_t hydrowInputAvailable = 0;
	uint8_t hydrowInputNewValue = 0;

    //control loop variables
    double CL_HYDROW;
    uint16_t CL_HYDROW_UINT;
    float CL_VSHUNT_K1;
    float CL_SAMPLE_ERROR;
    int16_t CL_DAC_ERROR;
    int16_t CL_CALC;
    uint16_t CL_PREV_DAC, CL_DAC_VALUE;
    float CL_K1, CL_K2, CL_K3; //scaling for control loop

    //poor mans ring buffer. Make an array that has 16 bits of space, use an index, let it rollover naturally
    //data_capture_t dataCaptureArray[256];
    //uint8_t dataCaptureArrayIndex;
	
    analogWriteResolution(10);
    //init DAC to 0
    analogWrite(DAC_OUTPUT_PIN, 0); //initialize to zero for safety

    Serial.begin(115200);  // pc
#ifdef DEBUG_TO_HYDROW_COM
	//Serial1.begin(115200); //hydrow
#else
	//Serial1.begin(921600); //hydrow
#endif

    INA237.ADDRESS = INA237_address;

    CL_HYDROW = 0;
    CL_HYDROW_UINT = 0;
    CL_VSHUNT_K1 = 0;
    CL_SAMPLE_ERROR = 0;
    CL_DAC_ERROR = 0;
    CL_PREV_DAC = 0;
    CL_DAC_VALUE = 0;

    CL_K1 = INA237_VS_DAC_SCALE_CONV;
    CL_K2 = 1;
    CL_K3 = HYDROW_DAC_SCALE_CONV;

    hydrowString = "";


#ifdef INA237_CONNECTED
    while (!ina237_init(&INA237, INA237_SHUNT_CAL))
    {
        snprintf(debugStringBuf, sizeof(debugStringBuf), "Couldn't init INA237!\n");
        Serial.print(debugStringBuf);

        TC3_Delay_ms(2000);
    }
#else
    ina237_init(&INA237, INA237_SHUNT_CAL);
#endif // INA237_CONNECTEeD




    snprintf(debugStringBuf, sizeof(debugStringBuf), "Begin...\n");
    Serial.print(debugStringBuf);

    snprintf(debugStringBuf, sizeof(debugStringBuf), "Type s and press enter to begin stream. Send e to end.\n");
    Serial.print(debugStringBuf);


    TC3_Delay_ms(2000);

    while (1)
    {
        if (timerCounterINA237 >= intervalINA237) //sample data
        {
            //if you reset the timer at the top then you maintain a fixed period...so long as other stuff doesn't get in the way,
            //and so long as the period is greater than the i2c access timing. 
            timerCounterINA237 = 0;

            //PORT_IOBUS->Group[0].OUTTGL.reg = PORT_PA14;


#ifdef INA237_CONNECTED
            if (!ina237_get_vshunt(&INA237)) break; //break if error
            if (!ina237_get_current(&INA237)) break; //break if error
#else
            ina237_get_vshunt(&INA237);
#endif // INA237_CONNECTED


            //read the VSHUNT measurement and put it into the same scale as the the 10 bit DAC
            //CL_VSHUNT_K1 = (INA237.readings.VSHUNT / CL_K1);  
            //CL_VSHUNT_K1 = (INA237.readings.VSHUNT / CL_K1);  
            CL_VSHUNT_K1 = (INA237.readings.VSHUNT / INA237_VS_DAC_SCALE_CONV); //divide by 32 to get us to the same resolution as the DAC
      //      
            //abs() doesn't work on floats, manually making positive
            if (CL_VSHUNT_K1 < 0) CL_VSHUNT_K1 *= -1.0;
			//todo: changed this to be less than zero to remove the sign from 0 which it was doing before

            //compare the hydrow reading and the not formatted VSHUNT and put that into the error variable
            //compare the commanded hydrow value against the current measurement
            CL_SAMPLE_ERROR = CL_HYDROW_UINT - CL_VSHUNT_K1;

            //Apply the scaling factor K2 to the sampled error. CL_K2 is where we make our tuning changes
            //cast into an int
            CL_DAC_ERROR = (uint16_t)(CL_SAMPLE_ERROR * CL_K2);

            //create a tmp variable so we can error check bounds
            CL_CALC = (CL_PREV_DAC + CL_DAC_ERROR);
            //the control loop has no idea of the output resolution, it purely increments or decrements
            //against the previous vales. Manually create bounds here.
            if (CL_CALC > 1023) CL_CALC = 1023;
            if (CL_CALC < 0) CL_CALC = 0;
            CL_DAC_VALUE = (uint16_t)CL_CALC;

            //store previous value
            //todo: may need to avera past values
            CL_PREV_DAC = CL_DAC_VALUE;

            //output new value to DAC
            analogWrite(DAC_OUTPUT_PIN, CL_DAC_VALUE);
			
            //dump values into ring buffer. Pre-increment so we're always operating on the most recent value for subsequent operations
            //dataCaptureArray[++dataCaptureArrayIndex] = (data_capture_t){ CL_HYDROW, CL_DAC_VALUE, (float)(INA237.readings.CURRENT * CURRENT_LSB) };
				
			//dataCaptureArrayIndex++;
			//dataCaptureArray[dataCaptureArrayIndex].currentReading = (float)(INA237.readings.CURRENT * CURRENT_LSB);
			//dataCaptureArray[dataCaptureArrayIndex].dacValue = CL_DAC_VALUE;
			//dataCaptureArray[dataCaptureArrayIndex].hydrowInput = CL_HYDROW;

			//dataCaptureArrayIndex++;
			//dataCapture.currentReading = (float)(INA237.readings.CURRENT * CURRENT_LSB);
			//dataCapture.dacValue = CL_DAC_VALUE;
			//dataCapture.hydrowInput = CL_HYDROW;
						

			//dataCaptureArrayIndex++;
			//dataCaptureArray[dataCaptureArrayIndex].currentReading = (float)(dataCaptureArrayIndex * CURRENT_LSB);
			//dataCaptureArray[dataCaptureArrayIndex].dacValue = dataCaptureArrayIndex;
			//dataCaptureArray[dataCaptureArrayIndex].hydrowInput = (float)dataCaptureArrayIndex;

			
            //dataCaptureArray[++dataCaptureArrayIndex] = (data_capture_t){ dataCaptureArrayIndex, dataCaptureArrayIndex * .69, dataCaptureArrayIndex * CURRENT_LSB };
        }

#ifdef DEBUG_READABLE
        if (timerCounterDEBUG > intervalDEBUG && DEBUG_DAC)
        {
            timerCounterDEBUG = 0;
			
			_dtostrf(CL_HYDROW, 5, 2, formattedPrint0);
			_dtostrf((float)(INA237.readings.CURRENT * CURRENT_LSB), 7, 3, formattedPrint1);
			snprintf(dataPrintBuf, sizeof(dataPrintBuf), "%s\t%u\t%s\n", formattedPrint0, CL_DAC_VALUE, formattedPrint1);
			

			
			//we fast as fuk boi
			//snprintf(dataPrintBuf, sizeof(dataPrintBuf), "%d.%02d\t%d\t%d.%02\n", (int)(dataCaptureArray[dataCaptureArrayIndex].hydrowInput), 
				//(int)(dataCaptureArray[dataCaptureArrayIndex].hydrowInput * 100) - (int)(dataCaptureArray[dataCaptureArrayIndex].hydrowInput * 100), 
				//dataCaptureArray[dataCaptureArrayIndex].dacValue, (int)(dataCaptureArray[dataCaptureArrayIndex].currentReading), 
				//(int)(dataCaptureArray[dataCaptureArrayIndex].currentReading * 100) - (int)(dataCaptureArray[dataCaptureArrayIndex].currentReading * 100));
            Serial.write(dataPrintBuf);

#ifdef DEBUG_TO_HYDROW_COM
			Serial1.write(dataPrintBuf);
#endif
			
			////while (!Serial1.availableForWrite());
			//
            ////_dtostrf(291.2, 5, 2, formattedPrint);
            //Serial1.write("291.2");
		    ////Serial1.print((float)29.2);
            ////Serial1.print(dataCaptureArray[dataCaptureArrayIndex].hydrowInput);
            ////Serial1.print("29.2");
			////while (!Serial1.availableForWrite());
			////sprintf(formattedPrint, "%d.%02d  ", (int)(dataCaptureArray[dataCaptureArrayIndex].hydrowInput), ((int)(dataCaptureArray[dataCaptureArrayIndex].hydrowInput * 100.0 + 0.5))%100) ;
            //Serial1.write("\t");
//
            //_dtostrf(1023, 4, 0, formattedPrint);
			//
            //Serial1.write(formattedPrint);
			////while (!Serial1.availableForWrite());
            ////Serial1.write(dataCaptureArray[dataCaptureArrayIndex].dacValue);
            ////Serial1.print("1023");
			////while (!Serial1.availableForWrite());
            //Serial1.write("\t");
//
            //_dtostrf(3.3, 7, 3, formattedPrint);
            //Serial1.write(formattedPrint);
            ////Serial1.print(dataCaptureArray[dataCaptureArrayIndex].currentReading);
			////while (!Serial1.availableForWrite());
            ////Serial1.print("3.3");
			////while (!Serial1.availableForWrite());
            //Serial1.write("\n");
        }
#endif // DEBUG_READABLE

		while (byteReceived && hydrowInputAvailable == 0)
		{
			readChar = receivedByte;
			byteReceived = 0;
			if (readChar != '\n')
			{
				hydrowCharArray[hydrowCharArrayIndex] = readChar;
				hydrowCharArrayIndex++;				
				
				//buffer size exceeded, move the counter back, just keep overwriting last byte until we hit a new line
				//leave an extra char to add back in the new line?
				if (hydrowCharArrayIndex >= 19) hydrowCharArrayIndex = 18;
			}
			else
			{
				hydrowCharArray[hydrowCharArrayIndex] = '\n';
				hydrowCharArray[hydrowCharArrayIndex + 1] = '\0';

				hydrowCharArrayIndex = 0;
				hydrowInputAvailable= 1;
			}
			
		}

		if (hydrowInputAvailable)
		{	
            CL_HYDROW = atof(hydrowCharArray);
            //CL_HYDROW = strtod(hydrowCharArray, '\0');

            //abs() doesn't work on floats, manually do abs
            if (CL_HYDROW < 0) CL_HYDROW *= -1.0;

            CL_HYDROW_UINT = (uint16_t)(CL_HYDROW * CL_K3);
				
			//Serial.print("Hydrow input received: ");
		//Serial.write(hydrowCharArray);
			//Serial.println(CL_HYDROW);
			
			while (Serial1.available()) Serial1.read(); //flush
			hydrowInputAvailable = 0;
		}

//
        ////hydrow input
        //if (Serial1.available())
        //{
            //hydrowCharArrayIndex = 0;
            //timeoutCounter = 0;
            //hydrowString = "";
            //readChar = NULL;
//
            ////wait for new line char
            //while (Serial1.available() && readChar != '\n' && timeoutCounter++ < timeoutMax)
            //{
				//readChar = Serial1.read();
//
                //
                ////hydrowString += readChar;
				//hydrowCharArray[hydrowCharArrayIndex++] = readChar; //indexing faster than string building
				//if (readChar == '\n')
				//{
					//Serial1.read();
				//}
            //}
//
//
//
            ////didn't hit a timeout
            //if (timeoutCounter < timeoutMax)
            //{
                ////finish building the "string" with a null terminator
                //hydrowCharArray[hydrowCharArrayIndex] = '\0';
//
                ////take in float value
                ////CL_HYDROW = hydrowString.toFloat();
				//
				////Serial.print("Hydrow input received: ");
				////Serial.println(CL_HYDROW);
				//
                //CL_HYDROW = atof(hydrowCharArray);
                ////CL_HYDROW = strtod(hydrowCharArray, '\0');
//
                ////abs() doesn't work on floats, manually do abs
                //if (CL_HYDROW < 0) CL_HYDROW *= -1.0;
//
                ////scale value into DAC range (CL_K3);
                ////CL_HYDROW_UINT = (uint16_t)(CL_HYDROW * CL_K3);
                //CL_HYDROW_UINT = (uint16_t)(CL_HYDROW * CL_K3);
				//
				////Serial.print("Hydrow input received: ");
				//Serial.println(CL_HYDROW);
//
            //}
//
            ////clear any remaining junk
            ////while (Serial1.available()) Serial1.read();
			////Serial1.begin(921600);
			//while (Serial1.available()) Serial1.read();
        //}


        ////read commands from debug stream
        if (debugInputComplete)
        {
            debugInputComplete = false; //reset flag



            if (debugString.charAt(0) == 'R')
            {
                CL_HYDROW = 0;
                CL_VSHUNT_K1 = 0;
                CL_SAMPLE_ERROR = 0;
                CL_DAC_ERROR = 0;
                CL_PREV_DAC = 0;
                CL_DAC_VALUE = 0;
                //CL_K1 = 0.2; //converts VSHUNT value to HYDROW format, mV to Nm
                //CL_K2 = ((2^dacRes) / 2 ) - 1; //these transistors don't conduct below 3.3/2, put the scaling factor for the DAC into upper half of range

                //CL_K2 = 1;

                analogWrite(DAC_OUTPUT_PIN, CL_DAC_VALUE);

                snprintf(debugStringBuf, sizeof(debugStringBuf), "RESET\n\n\n");
                Serial.print(debugStringBuf);

                snprintf(debugStringBuf, sizeof(debugStringBuf), "H\tVS\tS_E\tD_R\tDAC\tDAC_V\n", CL_VSHUNT_K1, CL_SAMPLE_ERROR, CL_DAC_ERROR, CL_DAC_VALUE);
                Serial.print(debugStringBuf);
            }

            if (debugString.charAt(0) == 's')
            {
                DEBUG_DAC = true;
                snprintf(debugStringBuf, sizeof(debugStringBuf), "Starting debugging stream...\n\n");
                Serial.print(debugStringBuf);
                // delay(500);

            }

            if (debugString.charAt(0) == 'e')
            {
                DEBUG_DAC = false;

                snprintf(debugStringBuf, sizeof(debugStringBuf), "Ending debugging stream...\n\n");
                Serial.print(debugStringBuf);
                //delay(500);
            }

            debugString = ""; //clear input string
        }

        //Commands from host PC
        if (Serial.available())
        {
            timeoutCounter = 0;
            readChar = NULL;

            /* get the new byte from buffer */
            readChar = (char)Serial.read();
            if (readChar == '\n') debugInputComplete = true;
            else debugString += readChar;
        }
    }
}

