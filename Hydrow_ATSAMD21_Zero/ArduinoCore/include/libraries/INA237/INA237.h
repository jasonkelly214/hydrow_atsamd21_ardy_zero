/*
 Name:		INA237.h
 Created:	2021-09-28 5:46:15 PM
 Author:	JasonKelly
 Editor:	http://www.visualmicro.com
*/

#ifndef _INA237_h
#define _INA237_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "Wire.h"

#define INA237_address 0x40
#define INA237_MANF_ID 0x5449
#define INA237_VS_MV_CONV 200
#define INA237_VS_DAC_SCALE_CONV 32
#define INA237_V_CONV 3.125
#define INA237_I_CONV 1
#define INA237_P_CONV (2 * INA237_I_CONV)
#define INA237_T_CONV 125

#define INA237_REG_CONFIG		0x00
#define INA237_REG_ADC_CONFIG	0x01
#define INA237_REG_SHUNT_CAL	0x02
#define INA237_REG_VSHUNT		0x04
#define INA237_REG_VBUS			0x05
#define INA237_REG_DIETEMP		0x06
#define INA237_REG_CURRENT		0x07
#define INA237_REG_POWER		0x08
#define INA237_REG_DIAG_ALRT	0x0B
#define INA237_REG_SOVL			0x0C
#define INA237_REG_SUVL			0x0D
#define INA237_REG_BOVL			0x0E
#define INA237_REG_BUVL			0x0F
#define INA237_REG_TEMP_LIMIT	0x10
#define INA237_REG_PWR_LIMIT	0x11
#define INA237_REG_MANF_ID		0x3E
#define INA237_REG_ERROR		0xFF

#define INA237_CONFIG_RST			15 //does a reset, self clears
#define INA237_CONFIG_CONVDLY		6	//8 bit value reflecting the delay for initial adc conversion. 2ms steps
#define INA237_CONFIG_ADCRANGE		4  //sets the adc range. 0 = +/- 163.84 mv; 1 = +/- 40.96 mv

#define INA237_ADC_CONFIG_MODE		12 //4-bit list of modes, see datasheet
#define INA237_ADC_CONFIG_VBUSCT	9  //3-bit list of conversion times - bus voltage measurement
#define INA237_ADC_CONFIG_VSHCT		6  //3-bit list of converion times - shunt voltage measurement
#define INA237_ADC_CONFIG_VTCT		3  //3-bit list of converion times - temperature measurement
#define INA237_ADC_CONFIG_AVG		0	//3-bit list of ADC sample averaging count

#define INA237_DIAG_ALRT_ALATCH		15
#define INA237_DIAG_ALRT_CNVR		14
#define INA237_DIAG_ALRT_SLOWALERT	13
#define INA237_DIAG_ALRT_APOL		12
#define INA237_DIAG_ALRT_MATHOF		9
#define INA237_DIAG_ALRT_TMPOL		7
#define INA237_DIAG_ALRT_SHNTOL		6
#define INA237_DIAG_ALRT_SHNTUL		5
#define INA237_DIAG_ALRT_BUSOL		4
#define INA237_DIAG_ALRT_BUSUL		3
#define INA237_DIAG_ALRT_POL		2
#define INA237_DIAG_ALRT_CNVRF		1
#define INA237_DIAG_ALRT_MEMSTAT	0

#define INA237_ADC_MODE_SHUTDOWN	0x00
#define INA237_ADC_MODE_VBUS		0x01
#define INA237_ADC_MODE_VSHUNT		0x02
#define INA237_ADC_MODE_TEMP		0x04
#define INA237_ADC_MODE_CONT		0x08

#define INA237_ADC_SAMPLE_50_US		0x00
#define INA237_ADC_SAMPLE_84_US		0x01
#define INA237_ADC_SAMPLE_150_US	0x02
#define INA237_ADC_SAMPLE_280_US	0x03
#define INA237_ADC_SAMPLE_540_US	0x04
#define INA237_ADC_SAMPLE_1052_US	0x05
#define INA237_ADC_SAMPLE_2074_US	0x06
#define INA237_ADC_SAMPLE_4120_US	0x07


typedef struct
{
	uint8_t ADDRESS;
	uint16_t MANF_ID;

	//struct
	//{
	//	uint16_t RST : 1;
	//	uint16_t CONVDLY : 8;
	//	uint16_t ADCRANGE : 1;
	//}CONFIG;
	uint16_t CONFIG_RAW;

	//struct
	//{
	//	uint16_t MODE : 4;
	//	uint16_t VBUSCT : 3;
	//	uint16_t VSHCT : 3;
	//	uint16_t VTCT : 3;
	//	uint16_t AVG : 3;
	//}ADC_CONFIG;
	uint16_t ADC_CONFIG_RAW;

	//struct 
	//{
	//	uint16_t ALATCH : 1;
	//	uint16_t CNVR : 1;
	//	uint16_t SLOWALERT : 1;
	//	uint16_t APOL : 1;
	//	uint16_t MATHOF : 1;
	//	uint16_t TMPOL : 1;
	//	uint16_t SHNTOL : 1;
	//	uint16_t SHNTUL : 1;
	//	uint16_t BUSOL : 1;
	//	uint16_t BUSUL : 1;
	//	uint16_t POL : 1;
	//	uint16_t CNVRF : 1;
	//	uint16_t MEMSTAT : 1;
	//}DIAG_ALRT;
	uint16_t DIAG_ALRT_RAW;

	uint16_t SHUNT_CAL; //0x1000 default

	struct
	{
		int16_t VBUS;
		int16_t VSHUNT;
		int16_t CURRENT;
		uint32_t POWER;
		int16_t DIETEMP;
	}readings;

	struct
	{
		int16_t SHUNT_OVER;		//0x7FFF default
		int16_t SHUNT_UNDER;	//0x8000 default
		uint16_t BUS_OVER;		//0x7FFF default
		uint16_t BUS_UNDER;		//0x0000 default
		int16_t TEMP_LIMIT;	//0x7FF0 default
		uint16_t POWER_LIMIT;	//0xFFFF default
	}limits;

}ina237_t;


bool ina237_reset(ina237_t* device);
bool ina237_init(ina237_t* device, uint16_t shuntValue);
bool ina237_get_shunt_cal(ina237_t* device);
bool ina237_set_shunt_cal(ina237_t* device, uint16_t value);
bool ina237_get_limits(ina237_t* device);
bool ina237_set_limits(ina237_t* device, int16_t SOVL, int16_t SUVL, uint16_t BOVL, uint16_t BUVL, int16_t TEMP, uint16_t PWR);
bool ina237_get_config(ina237_t* device);
bool ina237_set_config(ina237_t* device, uint16_t value);
bool ina237_get_diag_alrt(ina237_t* device);
bool ina237_set_diag_alrt(ina237_t* device, uint16_t value);
bool ina237_get_adc_config(ina237_t* device);
bool ina237_set_adc_config(ina237_t* device, uint16_t value);
bool ina237_get_vshunt(ina237_t* device);
bool ina237_get_vbus(ina237_t* device);
bool ina237_get_dietemp(ina237_t* device);
bool ina237_get_current(ina237_t* device);
bool ina237_get_power(ina237_t* device);
bool ina237_get_manf_id(ina237_t* device);
bool ina237_get_int16(uint8_t address, uint8_t pointerRegister, int16_t* value);
bool ina237_get_uint16(uint8_t address, uint8_t pointerRegister, uint16_t* value);
bool ina237_read_two_bytes(uint8_t address, uint16_t* value);
bool ina237_read_three_bytes(uint8_t address, uint32_t* value);
bool ina237_set_register(uint8_t address, uint8_t pointerRegister);
bool ina237_write_word(uint8_t address, uint8_t pointerRegister, uint16_t value);



#endif

