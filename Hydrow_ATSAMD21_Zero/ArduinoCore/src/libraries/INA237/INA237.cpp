/*
 Name:		INA237.cpp
 Created:	2021-09-28 5:46:15 PM
 Author:	JasonKelly
 Editor:	http://www.visualmicro.com
*/

#include "INA237.h"




bool ina237_reset(ina237_t* device)
{
	if (!ina237_set_config(device, 0x80)) return false; //reset command

	return true;
}

bool ina237_init(ina237_t* device, uint16_t shuntValue)
{
	Wire.begin();
	Wire.setClock(1000000);

	uint16_t configValue = 0x0000;

	//startup time is in microseconds. Hit a reset for safety, then let it ride
	ina237_reset(device);
	delay(50);

	if (!ina237_set_config(device, configValue)) return false;

	configValue = ((INA237_ADC_MODE_VSHUNT | INA237_ADC_MODE_VBUS | INA237_ADC_MODE_CONT) << INA237_ADC_CONFIG_MODE)
		| (INA237_ADC_SAMPLE_150_US << INA237_ADC_CONFIG_VBUSCT) | (INA237_ADC_SAMPLE_150_US << INA237_ADC_CONFIG_VSHCT)
		| (INA237_ADC_SAMPLE_150_US << INA237_ADC_CONFIG_VTCT) | (0x00 << INA237_ADC_CONFIG_AVG);
	if (!ina237_set_adc_config(device, configValue)) return false;

	if (!ina237_set_shunt_cal(device, shuntValue)) return false;

	if (!ina237_get_limits(device)) return false;

	return true;
}

bool ina237_get_shunt_cal(ina237_t* device)
{
	return ina237_get_uint16(device->ADDRESS, INA237_REG_SHUNT_CAL, &device->SHUNT_CAL);
}

bool ina237_set_shunt_cal(ina237_t* device, uint16_t value)
{
	if (!ina237_write_word(device->ADDRESS, INA237_REG_SHUNT_CAL, value)) return false;

	if (!ina237_get_shunt_cal(device)) return false;

	return value == device->SHUNT_CAL;
}


bool ina237_get_limits(ina237_t* device)
{
	if (!ina237_set_register(device->ADDRESS, INA237_REG_SOVL)) return false;
	if (!ina237_get_int16(device->ADDRESS, INA237_REG_SOVL, &device->limits.SHUNT_OVER)) return false;

	if (!ina237_set_register(device->ADDRESS, INA237_REG_SUVL)) return false;
	if (!ina237_get_int16(device->ADDRESS, INA237_REG_SUVL, &device->limits.SHUNT_UNDER)) return false;

	if (!ina237_set_register(device->ADDRESS, INA237_REG_BOVL)) return false;
	if (!ina237_get_uint16(device->ADDRESS, INA237_REG_BOVL, &device->limits.BUS_OVER)) return false;

	if (!ina237_set_register(device->ADDRESS, INA237_REG_BUVL)) return false;
	if (!ina237_get_uint16(device->ADDRESS, INA237_REG_BUVL, &device->limits.BUS_UNDER)) return false;

	if (!ina237_set_register(device->ADDRESS, INA237_REG_TEMP_LIMIT)) return false;
	if (!ina237_get_int16(device->ADDRESS, INA237_REG_TEMP_LIMIT, &device->limits.TEMP_LIMIT)) return false;

	if (!ina237_set_register(device->ADDRESS, INA237_REG_PWR_LIMIT)) return false;
	if (!ina237_get_uint16(device->ADDRESS, INA237_REG_PWR_LIMIT, &device->limits.POWER_LIMIT)) return false;

	return true;
}

bool ina237_set_limits(ina237_t* device, int16_t SOVL, int16_t SUVL, uint16_t BOVL, uint16_t BUVL, int16_t TEMP, uint16_t PWR)
{
	if (!ina237_write_word(device->ADDRESS, INA237_REG_SOVL, (uint16_t)SOVL)) return false;

	if (!ina237_write_word(device->ADDRESS, INA237_REG_SUVL, (uint16_t)SUVL)) return false;

	if (!ina237_write_word(device->ADDRESS, INA237_REG_BOVL, BOVL)) return false;

	if (!ina237_write_word(device->ADDRESS, INA237_REG_BUVL, BUVL)) return false;

	if (!ina237_write_word(device->ADDRESS, INA237_REG_TEMP_LIMIT, (uint16_t)TEMP)) return false;

	if (!ina237_write_word(device->ADDRESS, INA237_REG_PWR_LIMIT, PWR)) return false;

	return true;
}

bool ina237_get_config(ina237_t* device)
{
	if (!ina237_get_uint16(device->ADDRESS, INA237_REG_CONFIG, &device->CONFIG_RAW)) return false;

	//device->CONFIG.RST = (device->CONFIG_RAW >> INA237_CONFIG_RST) & 0x01;
	//device->CONFIG.CONVDLY = (device->CONFIG_RAW >> INA237_CONFIG_CONVDLY) & 0x0F;
	//device->CONFIG.ADCRANGE = (device->CONFIG_RAW >> INA237_CONFIG_ADCRANGE) & 0x01;
	return true;
}

bool ina237_set_config(ina237_t* device, uint16_t value)
{
	if (!ina237_write_word(device->ADDRESS, INA237_REG_CONFIG, value)) return false;

	if (!ina237_get_config(device));

	return value == device->CONFIG_RAW;
}

bool ina237_get_diag_alrt(ina237_t* device)
{

	if (!ina237_get_uint16(device->ADDRESS, INA237_REG_DIAG_ALRT, &device->DIAG_ALRT_RAW)) return false;

	//device->DIAG_ALRT.ALATCH	= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_ALATCH)	& 0x01;
	//device->DIAG_ALRT.CNVR		= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_CNVR)		& 0x01;
	//device->DIAG_ALRT.SLOWALERT = (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_SLOWALERT) & 0x01;
	//device->DIAG_ALRT.APOL		= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_APOL)		& 0x01;
	//device->DIAG_ALRT.MATHOF	= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_MATHOF)	& 0x01;
	//device->DIAG_ALRT.TMPOL		= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_TMPOL)		& 0x01;
	//device->DIAG_ALRT.SHNTOL	= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_SHNTOL)	& 0x01;
	//device->DIAG_ALRT.SHNTUL	= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_SHNTUL)	& 0x01;
	//device->DIAG_ALRT.BUSOL		= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_BUSOL)		& 0x01;
	//device->DIAG_ALRT.BUSUL		= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_BUSUL)		& 0x01;
	//device->DIAG_ALRT.POL		= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_POL)		& 0x01;
	//device->DIAG_ALRT.CNVRF		= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_CNVRF)		& 0x01;
	//device->DIAG_ALRT.MEMSTAT	= (device->DIAG_ALRT_RAW >> INA237_DIAG_ALRT_MEMSTAT)	& 0x01;

	return true;
}

bool ina237_set_diag_alrt(ina237_t* device, uint16_t value)
{
	/*uint16_t value;

	value = device->DIAG_ALRT.ALATCH << INA237_DIAG_ALRT_ALATCH | device->DIAG_ALRT.CNVR << INA237_DIAG_ALRT_CNVR
		| device->DIAG_ALRT.SLOWALERT << INA237_DIAG_ALRT_SLOWALERT | device->DIAG_ALRT.APOL << INA237_DIAG_ALRT_APOL
		| device->DIAG_ALRT.MATHOF << INA237_DIAG_ALRT_MATHOF | device->DIAG_ALRT.TMPOL << INA237_DIAG_ALRT_TMPOL
		| device->DIAG_ALRT.SHNTOL << INA237_DIAG_ALRT_SHNTOL | device->DIAG_ALRT.SHNTUL << INA237_DIAG_ALRT_SHNTUL
		| device->DIAG_ALRT.SHNTOL << INA237_DIAG_ALRT_SHNTOL | device->DIAG_ALRT.SHNTUL << INA237_DIAG_ALRT_SHNTUL
		| device->DIAG_ALRT.POL << INA237_DIAG_ALRT_POL | device->DIAG_ALRT.CNVRF << INA237_DIAG_ALRT_CNVRF
		| device->DIAG_ALRT.MEMSTAT << INA237_DIAG_ALRT_MEMSTAT;*/

	if (!ina237_write_word(device->ADDRESS, INA237_REG_DIAG_ALRT, value)) return false;

	if (!ina237_get_diag_alrt(device)) return false;

	return value == device->DIAG_ALRT_RAW;
}

bool ina237_get_adc_config(ina237_t* device)
{
	if (!ina237_get_uint16(device->ADDRESS, INA237_REG_ADC_CONFIG, &device->ADC_CONFIG_RAW)) return false;

	//device->ADC_CONFIG.MODE		= (device->ADC_CONFIG_RAW >> INA237_ADC_CONFIG_MODE)	& 0x04;
	//device->ADC_CONFIG.VBUSCT	= (device->ADC_CONFIG_RAW >> INA237_ADC_CONFIG_VBUSCT)	& 0x03;
	//device->ADC_CONFIG.VSHCT	= (device->ADC_CONFIG_RAW >> INA237_ADC_CONFIG_VSHCT)	& 0x03;
	//device->ADC_CONFIG.VTCT		= (device->ADC_CONFIG_RAW >> INA237_ADC_CONFIG_VTCT)	& 0x03;
	//device->ADC_CONFIG.AVG		= (device->ADC_CONFIG_RAW >> INA237_ADC_CONFIG_AVG)		& 0x03;

	return true;
}

bool ina237_set_adc_config(ina237_t* device, uint16_t value)
{/*
	uint16_t value;

	value = device->ADC_CONFIG.MODE << INA237_ADC_CONFIG_MODE | device->ADC_CONFIG.VBUSCT << INA237_ADC_CONFIG_VBUSCT
		| device->ADC_CONFIG.VSHCT << INA237_ADC_CONFIG_VSHCT | device->ADC_CONFIG.VTCT << INA237_ADC_CONFIG_VTCT
		| device->ADC_CONFIG.AVG << INA237_ADC_CONFIG_AVG;*/


	if (!ina237_write_word(device->ADDRESS, INA237_REG_ADC_CONFIG, value)) return false;

	if (!ina237_get_adc_config(device)) return false;

	return value == device->ADC_CONFIG_RAW;
}


//conversion factor is 5uv for ADCRANGE = 0, 1.25uv for 1.
bool ina237_get_vshunt(ina237_t* device)
{
	return ina237_get_int16(device->ADDRESS, INA237_REG_VSHUNT, &device->readings.VSHUNT);
}


//conversion factor is 3.125mv/LSB
bool ina237_get_vbus(ina237_t* device)
{
	return ina237_get_int16(device->ADDRESS, INA237_REG_VBUS, &device->readings.VBUS);
}

//temp is stored in upper 12 bits like TMP175. 125mC/lsb
bool ina237_get_dietemp(ina237_t* device)
{
	return ina237_get_int16(device->ADDRESS, INA237_REG_DIETEMP, &device->readings.DIETEMP);
}

//reported in amps due to conversion factor
bool ina237_get_current(ina237_t* device)
{
	return ina237_get_int16(device->ADDRESS, INA237_REG_CURRENT, &device->readings.CURRENT);
}

//reported in watts due to conversion factor. 
bool ina237_get_power(ina237_t* device)
{
	if (!ina237_set_register(device->ADDRESS, INA237_REG_POWER)) return false;

	//get the reading, return if failed
	return ina237_read_three_bytes(device->ADDRESS, &device->readings.POWER);
}

bool ina237_get_manf_id(ina237_t* device)
{
	return ina237_get_uint16(device->ADDRESS, INA237_REG_MANF_ID, &device->MANF_ID);
}


bool ina237_get_int16(uint8_t address, uint8_t pointerRegister, int16_t* value)
{
	uint16_t unsignedValue;

	if (!ina237_set_register(address, pointerRegister)) return false;

	//get the reading, return if failed
	if (!ina237_read_two_bytes(address, &unsignedValue)) return false;

	//value from the device is signed, cast it into the int
	*value = (int16_t)unsignedValue;

	return true;
}

bool ina237_get_uint16(uint8_t address, uint8_t pointerRegister, uint16_t* value)
{
	if (!ina237_set_register(address, pointerRegister)) return false;

	//get the reading, return if failed
	return ina237_read_two_bytes(address, value);
}


//register must be set beforehand
bool ina237_read_two_bytes(uint8_t address, uint16_t* value)
{
	//two bytes returned
	if (Wire.requestFrom(address, 2, true) != 2) return false;

	*value = Wire.read() << 8;
	*value |= Wire.read();
	return true;
}

//register must be set beforehand
bool ina237_read_three_bytes(uint8_t address, uint32_t* value)
{
	//three bytes returned
	if (Wire.requestFrom(address, 3, true) != 3) return false;

	*value = Wire.read() << 16;
	*value |= Wire.read() << 8;
	*value |= Wire.read();
	return true;
}

bool ina237_set_register(uint8_t address, uint8_t pointerRegister)
{
	uint8_t error;
	/*
	char buffer[40];
	sprintf(buffer, "Reg: %d Addr: %d\n", pointerRegister, address);
	SerialUSB.print(buffer);*/

	Wire.beginTransmission(address); //write
	Wire.write((byte)pointerRegister);
	error = Wire.endTransmission(true); //send stop

	return !error;
}

bool ina237_write_word(uint8_t address, uint8_t pointerRegister, uint16_t value)
{
	uint16_t returnedValue;

	Wire.beginTransmission(address); //write
	Wire.write((byte)(pointerRegister));
	Wire.write((byte)(value >> 8));
	Wire.write((byte)value);

	if (Wire.endTransmission(true) != 0) return false; //sends error code, return if nonzero

	return true;
}